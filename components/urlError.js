import { useToast } from "@chakra-ui/react";

export default function showError() {


  useToast({
    position: "top-right",
    status: "error",
    description: `Something Went Wrong`,
  })

}
