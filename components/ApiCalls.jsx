import axios from "axios";

const APIToken =
  "ATTA3b4afa91e29b410cdb9b58015ba52d7e5bea261bd6505960f5991fb019282247A3A85651";
const APIKey = "7d5923b2fb49db8f1a0d7ace94e5dc29";

export async function getMember(boardID) {
  const url = `https://api.trello.com/1/boards/${boardID}/memberships?key=${APIKey}&token=${APIToken}`;
}

export async function getBoardsFromBoardID(id) {
  const url = `https://api.trello.com/1/boards/${id}?key=${APIKey}&token=${APIToken}`;
  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getBoards() {
  const url = `https://api.trello.com/1/members/662f7b52db72821c6fa9c59e/boards?key=${APIKey}&token=${APIToken}`;
  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function createBoard(name) {
  const url = `https://api.trello.com/1/boards/?name=${name}&key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.post(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getListFromBoardID(id) {
  const url = `https://api.trello.com/1/boards/${id}/lists?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function createList(name, id) {
  const url = `https://api.trello.com/1/lists?idBoard=${id}&name=${name}&key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.post(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteList(listId) {
  const url = `https://api.trello.com/1/lists/${listId}/closed?key=${APIKey}&token=${APIToken}&value=true`;

  try {
    let response = await axios.put(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getCardFromListID(id) {
  const url = `https://api.trello.com/1/lists/${id}/cards?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function createCardByListID(name, listId) {
  const url = `https://api.trello.com/1/cards?idList=${listId}&name=${name}&key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.post(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteBoard(id) {
  const url = `https://api.trello.com/1/boards/${id}/closed?key=${APIKey}&token=${APIToken}&value=true`;

  try {
    let response = await axios.put(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteCard(id) {
  const url = `https://api.trello.com/1/cards/${id}/closed?key=${APIKey}&token=${APIToken}&value=true`;

  try {
    let response = await axios.put(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function createCheckListByCardID(name, cardId) {
  const url = `https://api.trello.com/1/checklists?idCard=${cardId}&key=${APIKey}&token=${APIToken}&name=${name}`;

  try {
    let response = await axios.post(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getChecklistFromCardID(cardId) {
  const url = `https://api.trello.com/1/cards/${cardId}/checklists?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteCheckList(id) {
  const url = `https://api.trello.com/1/checklists/${id}?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.delete(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getCheckItemsFromCheckListID(checkListId) {
  const url = `https://api.trello.com/1/checklists/${checkListId}/checkitems?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function createCheckItemFromCheckListID(name, checkListId) {
  const url = `https://api.trello.com/1/checklists/${checkListId}/checkitems?key=${APIKey}&token=${APIToken}&name=${name}`;

  try {
    let response = await axios.post(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function deleteCheckItem(checkItemID, checkListID) {
  const url = `https://api.trello.com/1/checklists/${checkListID}/checkItems/${checkItemID}?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.delete(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getCheckItemsFromCardID(cardID) {
  const url = `https://api.trello.com/1/cards/${cardID}/checklists?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function UpdatedCheckItem(checkItemID, cardID) {
  const url = `https://api.trello.com/1/cards/${cardID}/checkItem/${checkItemID}?key=${APIKey}&token=${APIToken}&state=complete`;

  try {
    let response = await axios.put(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function UnCheckedCheckItem(checkItemID, cardID) {
  const url = `https://api.trello.com/1/cards/${cardID}/checkItem/${checkItemID}?key=${APIKey}&token=${APIToken}&state=incomplete`;

  try {
    let response = await axios.put(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getCardFromCheckListID(checkListID) {
  const url = `https://api.trello.com/1/checkLists/${checkListID}/cards?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}

export async function getCardFromBoardID(boardID) {
  const url = `https://api.trello.com/1/boards/${boardID}/cards?key=${APIKey}&token=${APIToken}`;

  try {
    let response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
}
