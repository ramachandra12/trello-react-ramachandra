import React from 'react'
import {
    Flex,
    Heading,
    Button,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverHeader,
    PopoverBody,
    PopoverFooter,
    PopoverCloseButton,
  } from "@chakra-ui/react";
  import { faSquareCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ChecklistHeader({checkList, handleArchiveCheckList}) {
  return (
    <>
        <Flex justifyContent={"space-between"} gap={10} alignItems={"center"}>
        <Heading size="md">
          {" "}
          <FontAwesomeIcon icon={faSquareCheck}></FontAwesomeIcon>{" "}
          {checkList.name}
        </Heading>
        <Flex justify={"space-between"} alignItems={"center"}>
          <Popover placement="right">
            {({ onClose }) => (
              <>
                <PopoverTrigger>
                  <Button bg={"gray.400"} _hover={"none"}>
                    Delete
                  </Button>
                </PopoverTrigger>

                <PopoverContent w={"250px"}>
                  <PopoverCloseButton />
                  <PopoverHeader>{checkList.name}</PopoverHeader>
                  <PopoverBody>
                    Deleting a checklist is permanent and there is no way to get
                    it back.
                  </PopoverBody>
                  <PopoverFooter>
                    <Button
                      bg={"red"}
                      _hover={"none"}
                      onClick={() => {
                        handleArchiveCheckList(), onClose();
                      }}
                    >
                      {" "}
                      Archive this CheckList{" "}
                    </Button>
                  </PopoverFooter>
                </PopoverContent>
              </>
            )}
          </Popover>
        </Flex>
      </Flex>

    </>
  )
}
