import React, {useState } from 'react'
import { Box, Input, Button } from '@chakra-ui/react';


export default function AddCheckItem({handleAddCheckItem, checkItemName}) {
    const [isVisible, setIsVisible] = useState(true);
  return (
    <>
     {isVisible ? (
        <Box
          bg={"gray.200"}
          w={"150px"}
          onClick={() => (isVisible ? setIsVisible(false) : setIsVisible(true))}
          textAlign={"center"}
          _hover={{ bg: "gray.400" }}
          mt="10px"
          p={2}
        >
          + Add an item
        </Box>
      ) : (
        <>
         <form onSubmit={(event)=>{ handleAddCheckItem(event)}}>

          <Input
            placeholder="Enter the text"
            ref={checkItemName}
            mb={2}
            borderColor={"blue.400"}
            ></Input>
          <Button bg={"blue.400"} type='submit'>
            Add CheckItem
          </Button>
          <Button
            bg={"transparent"}
            onClick={() =>
              isVisible ? setIsVisible(false) : setIsVisible(true)
            }
            >
            cancel
          </Button>
            </form>
        </>
      )}
    </>
  )
}
