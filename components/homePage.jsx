import React, { useEffect, useRef, useContext } from "react";
import { Link } from "react-router-dom";
import { Box, Wrap, Heading, useToast } from "@chakra-ui/react";

import { HomePageTitle } from "./homePageComponents/homePageTitle";
import { getBoards, createBoard } from "./ApiCalls";
import boardContext from "./boardContext";
import { AddBoard } from "./homePageComponents/addBoard";

export default function HomePage() {
  const { boards, setBoards } = useContext(boardContext);

  const toast = useToast();

  function handleAddBoard(name) {
    createBoard(name)
      .then((data) => {
        setBoards([...boards, data]);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }
  
  useEffect(() => {
    getBoards()
      .then((data) => {
        const filteredBoards = data.filter((board) => !board.closed);
        setBoards(filteredBoards);
      })
      .catch((error) => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }, []);

  return (
    <>
      <Box maxW="1100px" mx="auto">
        <HomePageTitle />

        <Wrap spacing={10}>
          {boards.map((element) => {
            return (
              <Link to={`/boards/${element.id}`}>
                <Heading
                  borderRadius={"10px"}
                  fontSize={"xl"}
                  textColor={"white"}
                  w={"200px"}
                  h={"100px"}
                  backgroundSize="cover"
                  bg={
                    element.prefs.backgroundImage !== null
                      ? `url(${element.prefs.backgroundImage})`
                      : element.prefs.background
                  }
                  backgroundRepeat="no-repeat"
                  p={2}
                >
                  {element.name}
                </Heading>
              </Link>
            );
          })}
          <AddBoard handleAddBoard={handleAddBoard} />
        </Wrap>
      </Box>
    </>
  );
}
