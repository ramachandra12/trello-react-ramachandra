import React from "react";
import { Box, Flex, Heading, Text } from "@chakra-ui/react";
import { faPencil, faLock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function HomePageTitle() {
  return (
    <>
      <Box m="5rem">
        <Flex gap="5">
          <Box
            bg={"blue.600"}
            fontSize={"5xl"}
            w={16}
            h={16}
            textAlign={"center"}
            fontWeight={"bold"}
          >
            R
          </Box>
          <Box>
            <Heading>
              Rama Chandra's workspace &nbsp;
              <FontAwesomeIcon width={20} height={20} icon={faPencil}>
                {" "}
              </FontAwesomeIcon>
            </Heading>
            <Text>
              <FontAwesomeIcon icon={faLock}></FontAwesomeIcon> &nbsp; Private
            </Text>
          </Box>
        </Flex>
      </Box>
      <hr />
      <Heading fontWeight="bold" py="1rem" fontSize="2xl">
        Boards
      </Heading>
    </>
  );
}
