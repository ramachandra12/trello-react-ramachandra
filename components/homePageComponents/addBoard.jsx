import React, { useRef } from "react";
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverCloseButton,
  Button,
  Input,
} from "@chakra-ui/react";

export function AddBoard({ handleAddBoard }) {
  const newBoardName = useRef(null);

  function handleSubmit(event) {
    event.preventDefault();
    handleAddBoard(newBoardName.current.value);
    newBoardName.current.value = "";
  }

  return (
    <Popover initialFocusRef={newBoardName} returnFocusOnClose={false} placement="right">
      {({isOpen, onClose }) => (
        <>
          <PopoverTrigger>
            <Button
              w={"200px"}
              h={"100px"}
              bg="white"
              border={"1px"}
              _hover={"none"}
            >
              Add Board
            </Button>
          </PopoverTrigger>
          { isOpen && <PopoverContent w={"250px"} border={"1px"}>
            <PopoverCloseButton />
            <PopoverHeader>Enter the Board Name</PopoverHeader>
            <PopoverBody>
              <form onSubmit={(e)=>{handleSubmit(e);onClose() }}>
                <Input type="text" ref={newBoardName} borderColor={"red"} />
                <Button type="submit">Add Board</Button>
              </form>
            </PopoverBody>
          </PopoverContent>

          }
        </>
      )}
    </Popover>
  );
}
