import { Box, Button, Heading } from "@chakra-ui/react";
import { Link } from "react-router-dom";
function Error() {
  return (
    <Box display={"grid"} placeItems={"center"}>
      <Heading>No such Data found</Heading>
      <Link to={"/"}>
        <Button>Go to Home</Button>
      </Link>
    </Box>
  );
}

export default Error;
