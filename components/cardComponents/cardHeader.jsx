import React from "react";
import {
  Box,
  Flex,
  Button,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverCloseButton,
} from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsis } from "@fortawesome/free-solid-svg-icons";
import { faSquareCheck } from "@fortawesome/free-solid-svg-icons";

export default function CardHeader({
  card,
  handleArchiveCard,
  onOpen
}) {
  const totalCheckItem = card.badges.checkItems;
  const checkedCheckItem = card.badges.checkItemsChecked;
  return (
    <>
      <Flex justifyContent={"space-between"}>
        <p fontWeight="bold" onClick={onOpen}>
          {" "}
          {card.name}
        </p>

        <Popover placement="right">
          {({ onClose }) => (
            <>
              <PopoverTrigger>
                <Button bg={"transparent"} _hover={"none"}>
                  <FontAwesomeIcon icon={faEllipsis}></FontAwesomeIcon>
                </Button>
              </PopoverTrigger>
              <PopoverContent w={"200px"}>
                <PopoverCloseButton />
                <PopoverHeader>List Actions</PopoverHeader>
                <PopoverBody>
                  <Button
                    onClick={() => {
                      handleArchiveCard(card.id), onClose();
                    }}
                    bg={"red"}
                    _hover={"none"}
                  >
                    {" "}
                    Archive this Card{" "}
                  </Button>
                </PopoverBody>
              </PopoverContent>
            </>
          )}
        </Popover>
      </Flex>

      <Box
        bg={
          totalCheckItem !== 0 && totalCheckItem === checkedCheckItem
            ? "green"
            : "white"
        }
        w="50px"
        display="flex"
        justifyContent="center"
        alignItems="center"
        borderRadius="5px"
      >
        {totalCheckItem !== 0 && (
          <>
            <FontAwesomeIcon icon={faSquareCheck} />
            {checkedCheckItem}/{totalCheckItem}
          </>
        )}
      </Box>
    </>
  );
}
