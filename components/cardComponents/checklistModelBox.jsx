import React, { useRef, useState, useEffect } from "react";
import {
  Box,
  Flex,
  Input,
  Button,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverCloseButton,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  useToast,
} from "@chakra-ui/react";

import CheckLists from "../checkList";
import {
  getChecklistFromCardID,
  createCheckListByCardID,
  deleteCheckList,
} from "../ApiCalls";

export default function ChecklistModelBox({ card, isOpen, onOpen, onClose }) {
  const checkListName = useRef(null);
  const [checklists, setChecklists] = useState([]);
  const toast = useToast();

  function handleAddCheckList(event) {
    event.preventDefault();
    const name = checkListName.current.value;
    createCheckListByCardID(name, card.id)
      .then((data) => {
        setChecklists([...checklists, data]);
        checkListName.current.value = " ";
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }

  function handleArchiveCheckList(checkList) {
    deleteCheckList(checkList.id)
      .then((data) => {
        let filtered = checklists.filter((checklist) => {
          return checkList.id != checklist.id;
        });
        setChecklists(filtered);
      })
      .catch((error) => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong ${error.message} `,
        });
      });
  }

  useEffect(() => {
    getChecklistFromCardID(card.id)
      .then((res) => {
        setChecklists([...res]);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }, []);

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} size={"full"}>
        <ModalOverlay />
        <ModalContent
          p="1rem"
          width={"700px"}
          minH={"600px"}
          mt={20}
          borderRadius={"10px"}
        >
          <ModalHeader>{card.name}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex gap="5">
              <Box width={"80vh"}>
                {checklists.map((cList) => {
                  return (
                    <CheckLists
                      checkList={cList}
                      handleArchiveCheckList={handleArchiveCheckList}
                    />
                  );
                })}
              </Box>
              <Box width={"20vh"}>
                <p>Add to Card</p>
                <Popover placement="bottom">
                  {({ onClose }) => (
                    <>
                      <PopoverTrigger>
                        <Button
                          w={"150px"}
                          h={"30px"}
                          bg="white"
                          _hover={{ bg: "gray" }}
                          border={"1px"}
                        >
                          Add Checklist
                        </Button>
                      </PopoverTrigger>
                      <PopoverContent w={"250px"} border={"1px"}>
                        <PopoverCloseButton />
                        <PopoverHeader>Enter checklist </PopoverHeader>
                        <PopoverBody>
                          <form
                            action=""
                            onSubmit={(event) => {
                              handleAddCheckList(event), onClose();
                            }}
                          >
                            <label htmlFor="">Title</label>
                            <Input
                              type="text"
                              borderColor={"red"}
                              ref={checkListName}
                            />
                            <Button type="submit" bg={"blue.400"}>
                              Add{" "}
                            </Button>
                          </form>
                        </PopoverBody>
                      </PopoverContent>
                    </>
                  )}
                </Popover>
              </Box>
            </Flex>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
