import React, { useState } from "react";
import { Button, Input, Box } from "@chakra-ui/react";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function AddCard({ handleAddCard, cardName }) {
  const [isVisible, setIsVisible] = useState(true);
  return (
    <Box w={"200px"}>
      {isVisible ? (
        <Box
          w={"200px"}
          alignContent={"center"}
          h={"40px"}
          borderRadius="10px"
          onClick={() => (isVisible ? setIsVisible(false) : setIsVisible(true))}
          textAlign={"left"}
          _hover={{ bg: "gray.300" }}
        >
          + Add Card
        </Box>
      ) : (
        <>
          <form onSubmit={handleAddCard} >
            <Input
              placeholder="Enter Card Name"
              ref={cardName}
              mb={2}
              bg={"white"}
              w={"270px"}
              boxShadow="md"
            ></Input>
            <Button bg={"blue.400"} type="submit">
              Add Card
            </Button>
            <Button
              bg={"transparent"}
              onClick={() =>
                isVisible ? setIsVisible(false) : setIsVisible(true)
              }
            >
              <FontAwesomeIcon icon={faXmark}></FontAwesomeIcon>
            </Button>
          </form>
        </>
      )}
    </Box>
  );
}
