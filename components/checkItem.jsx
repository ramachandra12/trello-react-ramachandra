import React from "react";
import {
  Checkbox,
  Flex,
  Box,
  Button,
  Text,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverFooter,
  PopoverCloseButton,
} from "@chakra-ui/react";
import { faEllipsis } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function CheckItem({
  checkItem,
  handleArchiveCheckItem,
  handleUpdateCheckItem,
}) {
  return (
    <>
      <Flex justifyContent={"space-between"} alignItems={"center"}>
        <Box display={"flex"}>
          <Checkbox
            className="checkBox"
            w={"4px"}
            h={"4px"}
            isChecked={checkItem.state == "complete" ? true : false}
            onChange={(event) => {
              handleUpdateCheckItem(event, checkItem);
            }}
          >
            <Text
              textDecor={
                checkItem.state === "complete" ? "line-through" : "none"
              }
            >
              {checkItem.name}
            </Text>
          </Checkbox>
        </Box>
        <Popover placement="right">
          {({ onClose }) => (
            <>
              <PopoverTrigger>
                <Button bg={"white"}>
                  <FontAwesomeIcon icon={faEllipsis}></FontAwesomeIcon>
                </Button>
              </PopoverTrigger>

              <PopoverContent w={"250px"}>
                <PopoverCloseButton />
                <PopoverHeader>Items Actions</PopoverHeader>
                <PopoverFooter>
                  <Button
                    onClick={() => {
                      handleArchiveCheckItem(checkItem), onClose();
                    }}
                    bg={"red"}
                    _hover={"none"}
                  >
                    Archive checkItem
                  </Button>
                </PopoverFooter>
              </PopoverContent>
            </>
          )}
        </Popover>
      </Flex>
    </>
  );
}
