import React from "react";
import { Box, useDisclosure, useToast } from "@chakra-ui/react";


import CardHeader from "./cardComponents/cardHeader";
import ChecklistModelBox from "./cardComponents/checklistModelBox";

function CreateCard({ card, handleArchiveCard }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Box
        _hover={{ border: "1px", borderColor: "blue.400" }}
        my={2}
        borderRadius={"10px"}
        alignItems={"center"}
        boxShadow="md"
        p="2"
        bg="white"
      >
        <CardHeader
          key={card}
          card={card}
          handleArchiveCard={handleArchiveCard}
          onOpen={onOpen}
        />
      </Box>
      {isOpen ? (
        <ChecklistModelBox
          key={card}
          isOpen={isOpen}
          onClose={onClose}
          onOpen={onOpen}
          card={card}
        />
      ) : null}
    </>
  );
}

export default CreateCard;
