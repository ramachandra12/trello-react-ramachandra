import { Box, Text, Image } from "@chakra-ui/react";
import React from "react";

export default function Header() {
  return (
    <Box bg={"black"} h={"10vh"} display="flex" justifyContent={"center"}>
      <Image h={"8vh"} src="/trello-logo-blue.png"></Image>
    </Box>
  );
}
