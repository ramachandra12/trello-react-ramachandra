import React from "react";

import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverCloseButton,
  Button,
  Input,
  Text,
  Flex,
} from "@chakra-ui/react";

export default function ListHeader({ name, handleArchiveList }) {
  return (
    <>
      <Flex justify={"space-between"} alignItems={"center"}>
        <Text>{name}</Text>
        <Popover placement="right">
          {({ onClose }) => (
            <>
              <PopoverTrigger>
                <Button bg={"transparent"} _hover={"none"}>
                  ...
                </Button>
              </PopoverTrigger>
              <PopoverContent w={"200px"}>
                <PopoverCloseButton />
                <PopoverHeader>List Actions</PopoverHeader>
                <PopoverBody>
                  <Button
                    onClick={() => {
                      handleArchiveList(), onClose();
                    }}
                    bg={"red"}
                    _hover={"none"}
                  >
                    {" "}
                    Archive this list{" "}
                  </Button>
                </PopoverBody>
              </PopoverContent>
            </>
          )}
        </Popover>
      </Flex>
    </>
  );
}
