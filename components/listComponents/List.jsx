import React, { useEffect, useRef, useState } from "react";
import { Box, useToast } from "@chakra-ui/react";

import CreateCard from "../card";
import { getCardFromListID, createCardByListID, deleteCard } from "../ApiCalls";
import CardContext from "../CardContext";
import ListHeader from "./listHeader";
import AddCard from "../cardComponents/addCard";

export default function List({ name, listID, handleArchiveList }) {
  const toast = useToast();
  const [cardDetails, setCardDetails] = useState([]);
  const cardName = useRef(null);

  function updateCheckedCheckItemsCards(status, checkList, count = 1) {
    const updatedCardDetails = cardDetails.map((card) => {
      if (card.id === checkList.idCard) {
        const updatedCard = { ...card };
        if (status === "complete") {
          updatedCard.badges.checkItemsChecked += count;
        } else {
          updatedCard.badges.checkItemsChecked -= count;
        }
        return updatedCard;
      }
      return card;
    });
    setCardDetails(updatedCardDetails);
  }

  function updateTotalItemsInCard(checkList, state, count = 1) {
    let reqCard = cardDetails.find((card) => card.id === checkList.idCard);
    reqCard = { ...reqCard };
    if (state == true) {
      reqCard.badges.checkItems -= count;
    } else {
      reqCard.badges.checkItems += count;
    }
    reqCard.badges;
    setCardDetails(
      cardDetails.map((card) => {
        if (card.id === reqCard.id) {
          return reqCard;
        }
        return card;
      })
    );
  }

  function handleAddCard(event) {
    event.preventDefault();
    createCardByListID(cardName.current.value, listID)
      .then((data) => {
        setCardDetails([...cardDetails, data]);
      })
      .catch((error) => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
    cardName.current.value = "";
  }

  function handleArchiveCard(cardId) {
    deleteCard(cardId)
      .then((data) => {
        let filtered = cardDetails.filter((card) => {
          return data.id != card.id;
        });
        setCardDetails(filtered);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }

  useEffect(() => {
    getCardFromListID(listID)
      .then((result) => {
        setCardDetails(result);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }, []);

  return (
    <div paddingTop={10}>
      <Box
        w={"300px"}
        border={"1px"}
        borderRadius="10px"
        bg={"whitesmoke"}
        p={3}
      >
        <ListHeader
          key={name}
          name={name}
          handleArchiveList={() => {
            handleArchiveList(listID);
          }}
        />

        <Box my={"1rem"}>
          <CardContext.Provider
            value={{ updateCheckedCheckItemsCards, updateTotalItemsInCard }}
          >
            {cardDetails.map((card) => {
              return (
                <CreateCard
                  key={card}
                  card={card}
                  handleArchiveCard={handleArchiveCard}
                ></CreateCard>
              );
            })}
          </CardContext.Provider>
        </Box>

        <AddCard
          key={cardName}
          handleAddCard={handleAddCard}
          cardName={cardName}
        />
      </Box>
    </div>
  );
}
