import React, { useState, useEffect, useRef } from "react";
import { Flex, useToast } from "@chakra-ui/react";

import List from "./List";
import AddList from "./AddAnotherList";
import { getListFromBoardID, deleteList, createList } from "../ApiCalls";

export default function ListArea({ boardID }) {
  const [lists, setLists] = useState([]);
  const toast = useToast();
  const listName = useRef(null);

  useEffect(() => {
    getListFromBoardID(boardID)
      .then((result) => {
        setLists(result);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }, []);

  function handleArchiveList(listID) {
    deleteList(listID)
      .then((data) => {
        let filterList = lists.filter((list) => list.id != data.id);
        setLists(filterList);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }

  function handleAddList(event, id) {
    event.preventDefault();
    createList(listName.current.value, id)
      .then((data) => {
        setLists([...lists, data]);
        listName.current.value = "";
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }

  return (
    <Flex gap={5} padding={3} overflowX="auto">
      <Flex gap={5} spacing={10}>
        {lists.map((listData) => (
          <List
            key={listData.id}
            name={listData.name}
            listID={listData.id}
            handleArchiveList={handleArchiveList}
          />
        ))}
        <AddList
          key={listName}
          handleAddList={handleAddList}
          listName={listName}
        />
      </Flex>
    </Flex>
  );
}
