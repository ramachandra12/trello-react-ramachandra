import { useState } from "react";
import { useParams } from "react-router-dom";
import { Box, Input, Button } from "@chakra-ui/react";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function AddList({ handleAddList, listName }) {
  const { id } = useParams();
  const [isVisible, setIsVisible] = useState(true);

  return (
    <Box
      w={"300px"}
      border={"1px"}
      h={"fit-content"}
      borderRadius="10px"
      bg={"whitesmoke"}
      p={4}
    >
      {isVisible ? (
        <Box
          w={"200px"}
          onClick={() => (isVisible ? setIsVisible(false) : setIsVisible(true))}
          textAlign={"center"}
        >
          + Add Another List
        </Box>
      ) : (
        <>
          <form
            action=""
            onSubmit={(event) => {
              handleAddList(event, id);
            }}
          >
            <Input placeholder="Enter the text" ref={listName} required></Input>
            <Button bg={"blue.400"} type={"submit"}>
              Add List
            </Button>{" "}
            <Button
              bg={"transparent"}
              onClick={() =>
                isVisible ? setIsVisible(false) : setIsVisible(true)
              }
            >
              <FontAwesomeIcon icon={faXmark}></FontAwesomeIcon>
            </Button>
          </form>
        </>
      )}
    </Box>
  );
}
