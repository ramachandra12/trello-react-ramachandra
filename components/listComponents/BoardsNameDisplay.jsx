import React, { useContext, useRef, useState } from "react";
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverCloseButton,
  Box,
  Flex,
  Text,
  Heading,
  Button,
  Input,
  useToast,
} from "@chakra-ui/react";
import { faAdd, faEllipsis } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

import { createBoard } from "../ApiCalls";
import boardContext from "../boardContext";

export default function BoardsNameDisplay({
  handleDeleteBoard,
  handleBoardClick,
}) {
  const { boards, setBoards } = useContext(boardContext);
  const newBoardName = useRef(null);
  const toast = useToast();

  function handleAddBoard(event) {
    event.preventDefault();
    createBoard(newBoardName.current.value)
      .then((data) => {
        let a = data;
        setBoards([...boards, a]);
        newBoardName.current.value = "";
      })
      .catch(() => {
        toast({
          position: "top-right",
          text: "something went wrong",
        });
      });
  }

  return (
    <Box bg="hsla(0, 0%, 100%, 0.16)" minW={"12rem"} minH={"90vh"} p={2}>
      <Flex justifyContent={"space-between"} alignItems={"center"}>
        <Heading fontSize={"2xl"}>Your Boards </Heading>
        <Popover
          placement="right"
          initialFocusRef={newBoardName}
          returnFocusOnClose={false}
        >
          {({ isOpen, onClose }) => (
            <>
              <PopoverTrigger>
                <FontAwesomeIcon icon={faAdd}></FontAwesomeIcon>
              </PopoverTrigger>
              {isOpen && (
                <PopoverContent w={"250px"} border={"1px"}>
                  <PopoverCloseButton />
                  <PopoverHeader>Enter the Board Name</PopoverHeader>
                  <PopoverBody>
                    <form
                      onSubmit={(event) => {
                        handleAddBoard(event), onClose();
                      }}
                    >
                      <Input
                        type="text"
                        ref={newBoardName}
                        borderColor={"red"}
                      />
                      <Button type="submit">Add Board</Button>
                    </form>
                  </PopoverBody>
                </PopoverContent>
              )}
            </>
          )}
        </Popover>
      </Flex>
      {boards.map((element) => {
        return (
          <Flex className="boardNamePopUp" justifyContent={"space-between"}>
            <Link to={`/boards/${element.id}`}>
              <Text
                fontSize={"xl"}
                onClick={() => {
                  handleBoardClick(element.id);
                }}
              >
                {element.name}{" "}
              </Text>
            </Link>
            <Popover placement="right">
              {({ onClose }) => (
                <>
                  <PopoverTrigger>
                    <Button
                      bg={"transparent"}
                      _hover={"none"}
                      display={"none"}
                      className="deleteBoardPopup"
                    >
                      <FontAwesomeIcon icon={faEllipsis}></FontAwesomeIcon>
                    </Button>
                  </PopoverTrigger>
                  <PopoverContent w={"200px"}>
                    <PopoverCloseButton />
                    <PopoverHeader>{element.name}</PopoverHeader>
                    <PopoverBody>
                      <Button
                        onClick={() => {
                          handleDeleteBoard(element.id), onClose();
                        }}
                        bg={"red"}
                        _hover={"none"}
                      >
                        Archive this Board{" "}
                      </Button>
                    </PopoverBody>
                  </PopoverContent>
                </>
              )}
            </Popover>
          </Flex>
        );
      })}
    </Box>
  );
}
