import React, { useEffect, useRef, useState, useContext } from "react";
import { Flex, Progress, useToast } from "@chakra-ui/react";

import {
  getCheckItemsFromCheckListID,
  createCheckItemFromCheckListID,
  deleteCheckItem,
  UpdatedCheckItem,
  UnCheckedCheckItem,
} from "./ApiCalls";
import CardContext from "./CardContext";
import CheckItem from "./checkItem";
import ChecklistHeader from "./checklistcomponents/checklistHeader";
import AddCheckItem from "./checklistcomponents/addcheckItem";

export default function CheckLists({ checkList, handleArchiveCheckList }) {
  const { updateCheckedCheckItemsCards, updateTotalItemsInCard } =
    useContext(CardContext);
  const [checkItems, setCheckItems] = useState([]);
  const checkItemName = useRef(null);
  const toast = useToast();

  const totalCount = checkItems.length;
  const completedCount = checkItems.filter(
    (item) => item.state === "complete"
  ).length;

  function handleArchiveCheckItem(checkItem) {
    if (checkItem.state == "complete") {
      updateCheckedCheckItemsCards(!checkItem.state, checkList);
    }
    updateTotalItemsInCard(checkList, true);

    deleteCheckItem(checkItem.id, checkItem.idChecklist)
      .then((data) => {
        const filterData = checkItems.filter((data) => {
          return data.id != checkItem.id;
        });
        setCheckItems(filterData);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }

  function handleUpdateCheckItem(event, checkItem) {
    const isChecked = event.target.checked;
    const updateFunction = isChecked ? UpdatedCheckItem : UnCheckedCheckItem;
    updateFunction(checkItem.id, checkList.idCard)
      .then(() => {
        const updatedCheckItems = checkItems.map((item) => {
          if (item.id === checkItem.id) {
            item.state = isChecked ? "complete" : "incomplete";
          }
          return item;
        });
        setCheckItems(updatedCheckItems);
        updateCheckedCheckItemsCards(
          isChecked ? "complete" : "incomplete",
          checkList
        );
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `Something went wrong`,
        });
      });
  }

  function handleAddCheckItem(event) {
    event.preventDefault();
    createCheckItemFromCheckListID(checkItemName.current.value, checkList.id)
      .then((data) => {
        setCheckItems([...checkItems, data]);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
    checkItemName.current.value = " ";
    updateTotalItemsInCard(checkList, false);
  }

  function handleUpdateCheckList() {
    updateTotalItemsInCard(checkList, true, checkItems.length);
    updateCheckedCheckItemsCards("inComplete", checkList, completedCount);
    handleArchiveCheckList(checkList);
  }

  useEffect(() => {
    getCheckItemsFromCheckListID(checkList.id)
      .then((data) => {
        console.log(data);
        setCheckItems(data);
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }, []);

  return (
    <>
      <ChecklistHeader
        checkList={checkList}
        handleArchiveCheckList={handleUpdateCheckList}
      />

      <Flex gap={10} alignItems="center">
        {totalCount !== 0
          ? `${Math.floor((completedCount / totalCount) * 100)}%`
          : `0%`}
        <Progress
          value={totalCount != 0 ? (completedCount / totalCount) * 100 : 0}
          w={"100%"}
          my={5}
          h={3}
          borderRadius={"10px"}
        />
      </Flex>

      {checkItems.map((checkItem) => {
        return (
          <CheckItem
            checkItem={checkItem}
            handleArchiveCheckItem={handleArchiveCheckItem}
            handleUpdateCheckItem={handleUpdateCheckItem}
          />
        );
      })}
      <AddCheckItem
        handleAddCheckItem={handleAddCheckItem}
        checkItemName={checkItemName}
      />
    </>
  );
}
