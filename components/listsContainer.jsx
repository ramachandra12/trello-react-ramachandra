import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Flex, Heading, Spinner, useToast } from "@chakra-ui/react";

import ListArea from "./listComponents/ListArea";
import BoardsNameDisplay from "./listComponents/BoardsNameDisplay";
import Error from "./error";
import boardContext from "./boardContext";
import { getBoards, deleteBoard } from "./ApiCalls";
import NoBoard from "./noBoard";

export default function Lists() {
  const { boards, setBoards } = useContext(boardContext);
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [boardData, setBoardData] = useState(null);
  const [error, setError] = useState(false);
  const toast = useToast();

  function handleDeleteBoard(boardID) {
    deleteBoard(boardID)
      .then((data) => {
        let filtered = boards.filter((board) => data.id !== board.id);
        setBoards(filtered);
        if (id == boardID) {
          setBoardData(null);
        }
      })
      .catch(() => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }

  function handleBoardClick(boardId) {
    console.log(boardId);
    let filtered = boards.find((board) => board.id === boardId);
    setBoardData(filtered);
  }

  useEffect(() => {
    getBoards()
      .then((data) => {
        const filteredBoards = data.filter((board) => !board.closed);
        setBoards(filteredBoards);
        const foundBoard = filteredBoards.find((board) => board.id === id);
        if (foundBoard) {
          setBoardData(foundBoard);
        } else {
          setError(true);
        }
        setLoading(false);
      })
      .catch((error) => {
        toast({
          position: "top-right",
          status: "error",
          description: `something went wrong `,
        });
      });
  }, []);

  if (loading) {
    return (
      <Heading textAlign={"center"}>
        Loading <Spinner></Spinner>
      </Heading>
    );
  }

  if (error) {
    return <Error />;
  }
  if (!boardData) {
    return (
      <Flex bg={"blue.400"} backgroundSize="cover" backgroundRepeat="no-repeat">
        <BoardsNameDisplay
          handleBoardClick={handleBoardClick}
          handleDeleteBoard={handleDeleteBoard}
        />
        <NoBoard />
      </Flex>
    );
  }

  return (
    <Flex
      bg={
        boardData.prefs.backgroundImage
          ? `url(${boardData.prefs.backgroundImage})`
          : boardData.prefs.background
      }
      backgroundSize="cover"
      backgroundRepeat="no-repeat"
    >
      <BoardsNameDisplay
        handleBoardClick={handleBoardClick}
        handleDeleteBoard={handleDeleteBoard}
      />
      <ListArea key={id} boardID={id} />
    </Flex>
  );
}
