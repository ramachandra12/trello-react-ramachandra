import { Box, Heading, Flex } from "@chakra-ui/react";
import React from "react";

export default function NoBoard() {
  return (
    <Flex mx={"auto"} alignContent={"center"}>
      <Heading>This board is closed</Heading>
    </Flex>
  );
}
