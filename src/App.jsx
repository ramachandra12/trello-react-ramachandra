import { useState } from "react";
import { useToast } from "@chakra-ui/react";
import { BrowserRouter, Route, Routes} from "react-router-dom";

import Lists from "../components/listsContainer";
import HomePage from "../components/homePage";
import boardContext from "../components/boardContext";
import Header from "../components/header"
import Error from "../components/error";

import './App.css'

function App() {
  const [boards, setBoards] = useState([]);
  
  return (
    <>
  <Header/>
   <boardContext.Provider value={{boards, setBoards}}>
   <BrowserRouter>
   <Routes>
    <Route path="/" element={<HomePage/> }></Route>
     <Route path="/boards/:id"  key={Lists} element={<Lists/>}></Route>
     <Route path="*" element={<Error/>}></Route>
   </Routes>
   </BrowserRouter>
   </boardContext.Provider>
  
   </>
  )
}

export default App
